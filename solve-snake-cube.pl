%%% SWI Prolog programm to solve the snake cube puzzle %%%
% See also: https://www.instructables.com/id/Snake-Cube-Puzzle-Solution/
% Run programm via the follwoing swi prolog command:
% run(R).
% It should produce the follwing output:
% R = ['1-x', '2-y', '3x', '4-z', '5-y', '6z', '7-x', '8-z', '9y'|...] 
% Pressing they ";" key several times gives you all possible solution for the snake cube puzzle:
% R = ['1-x', '2-z', '3x', '4-y', '5-z', '6y', '7-x', '8-y', '9z'|...] ;
% R = ['1-y', '2-x', '3y', '4-z', '5-x', '6z', '7-y', '8-z', '9x'|...] ;
% R = ['1-y', '2-z', '3y', '4-x', '5-z', '6x', '7-y', '8-x', '9z'|...] ;
% R = ['1-z', '2-x', '3z', '4-y', '5-x', '6y', '7-z', '8-y', '9x'|...] ;
% R = ['1-z', '2-y', '3z', '4-x', '5-y', '6x', '7-z', '8-x', '9y'|...] ;
% false.

run(Result):-
	cube(0,[2,1,1,2,1,2,1,1,2,2,1,1,1,2,2,2,2],[112,113,121,122,123,131,132,133,211,212,213,221,222,223,231,232,233,311,312,313,321,322,323,331,332,111],333,Result).


cube(Counter,[FirstElem|Snake],StillFree,Current,[FirstOutput|OutputList]) :-
	Counter1 is Counter+1,
	isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis),
	concat(Counter1,Axis,FirstOutput),
	cube(Counter1,Snake,NextStillFree,NextCurrent,OutputList).
	
%Abbruchbedingung	
cube(_,[],_,_,[]).

%Axis x	one Element
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=1,
	TmpCurrent is Current+1,
	TmpCurrent mod 10 < 4,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	NextCurrent is TmpCurrent,
	append(StillFree2,[],NextStillFree),
	Axis='x'.
%Axis x	two Elements
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=2,
	TmpCurrent is Current+1,
	TmpCurrent mod 10 < 4,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	TmpCurrent2 is TmpCurrent+1,
	TmpCurrent2 mod 10 < 4,
	member(TmpCurrent2,StillFree2),
	select(TmpCurrent2,StillFree2,StillFree3),
	NextCurrent is TmpCurrent2, 
	append(StillFree3,[],NextStillFree),
	Axis='x'.	
%Axis -x one Element
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=1,
	TmpCurrent is Current-1,
	TmpCurrent mod 10 > 0,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	NextCurrent is TmpCurrent,
	append(StillFree2,[],NextStillFree),
	Axis='-x'.
%Axis -x two Elements
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=2,
	TmpCurrent is Current-1,
	TmpCurrent mod 10 > 0,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	TmpCurrent2 is TmpCurrent-1,
	TmpCurrent2 mod 10 > 0,
	member(TmpCurrent2,StillFree2),
	select(TmpCurrent2,StillFree2,StillFree3),
	NextCurrent is TmpCurrent2, 
	append(StillFree3,[],NextStillFree),
	Axis='-x'.
	
%Axis y one Element
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=1,
	TmpCurrent is Current+10,
	floor(TmpCurrent/10) mod 10 < 4,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	NextCurrent is TmpCurrent,
	append(StillFree2,[],NextStillFree),
	Axis='y'.
%Axis y two Elements
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=2,
	TmpCurrent is Current+10,
	floor(TmpCurrent/10) mod 10 < 4,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	TmpCurrent2 is TmpCurrent+10,
	floor(TmpCurrent2/10) mod 10 < 4,
	member(TmpCurrent2,StillFree2),
	select(TmpCurrent2,StillFree2,StillFree3),
	NextCurrent is TmpCurrent2, 
	append(StillFree3,[],NextStillFree),
	Axis='y'.	
%Axis y one Elements
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=1,
	TmpCurrent is Current-10,
	floor(TmpCurrent/10) mod 10 > 0,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	NextCurrent is TmpCurrent,
	append(StillFree2,[],NextStillFree),
	Axis='-y'.
%Axis y two Elements
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=2,
	TmpCurrent is Current-10,
	floor(TmpCurrent/10) mod 10 > 0,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	TmpCurrent2 is TmpCurrent-10,
	floor(TmpCurrent2/10) mod 10 > 0,
	member(TmpCurrent2,StillFree2),
	select(TmpCurrent2,StillFree2,StillFree3),
	NextCurrent is TmpCurrent2, 
	append(StillFree3,[],NextStillFree),
	Axis='-y'.	
	
%Axis z one Element
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=1,
	TmpCurrent is Current+100,
	floor(TmpCurrent/100) < 4,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	NextCurrent is TmpCurrent,
	append(StillFree2,[],NextStillFree),
	Axis='z'.
%Axis z two Elements
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=2,
	TmpCurrent is Current+100,
	floor(TmpCurrent/100) < 4,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	TmpCurrent2 is TmpCurrent+100,
	floor(TmpCurrent2/100) < 4,
	member(TmpCurrent2,StillFree2),
	select(TmpCurrent2,StillFree2,StillFree3),
	NextCurrent is TmpCurrent2, 
	append(StillFree3,[],NextStillFree),
	Axis='z'.
	
%Axis -z one Element
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=1,
	TmpCurrent is Current-100,
	floor(TmpCurrent/100) > 0,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	NextCurrent is TmpCurrent,
	append(StillFree2,[],NextStillFree),
	Axis='-z'.
%Axis -z two Elements
isFree(FirstElem,Current,NextCurrent,StillFree,NextStillFree,Axis):-
	FirstElem=2,
	TmpCurrent is Current-100,
	floor(TmpCurrent/100) > 0,
	member(TmpCurrent,StillFree),
	select(TmpCurrent,StillFree,StillFree2),
	TmpCurrent2 is TmpCurrent-100,
	floor(TmpCurrent2/100) > 0,
	member(TmpCurrent2,StillFree2),
	select(TmpCurrent2,StillFree2,StillFree3),
	NextCurrent is TmpCurrent2, 
	append(StillFree3,[],NextStillFree),
	Axis='-z'.