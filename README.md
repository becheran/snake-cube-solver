# Snake cube solver

Fun [SWI-Prolog](http://www.swi-prolog.org/) code to solve the [snake cube puzzle](https://www.instructables.com/id/Snake-Cube-Puzzle-Solution/) via formal logic. I wrote this code for fun during my knowledge-based systems lecture at the Technische Hochschule Ingolstadt.

## Problem description
The snake cube puzzle is a string of wood cubes which can be rotated at the endpoints. The goal of the puzzle is to rotate the snake of elements so that one gets a 3x3x3-Cube. The snake consists of 17 individual "elements"  which have the following layout:

![snake_cube_layout](/doc/snake_cube_layout.png)


## Quick Start
Download and install [SWI-Prolog](http://www.swi-prolog.org/). Then load the solver file into Prolog. Just double click or open file on Linux with:

    swipl -s solve-snake-cube.pl

Simply run the code via the following command:

    run(R).
    R = ['1-x', '2-y', '3x', '4-z', '5-y', '6z', '7-x', '8-z', '9y'|...] 

Not that not the full output of the list is given. You have to press the *w* key to print the full output:

    R = ['1-x', '2-y', '3x', '4-z', '5-y', '6z', '7-x', '8-z', '9y'|...] [write]
    R = ['1-x', '2-y', '3x', '4-z', '5-y', '6z', '7-x', '8-z', '9y', '10x', '11-y', '12z', '13-y', '14-z', '15y', '16-x', '17-y'] ;

Since more than one solution exists you can press the *;* or *tab* key after each result until Prolog outputs false and the program ends. This gives you all possible answers for solving the snake cube puzzle. 

    R = ['1-x', '2-z', '3x', '4-y', '5-z', '6y', '7-x', '8-y', '9z', '10x', '11-z', '12y', '13-z', '14-y', '15z', '16-x', '17-z'] ;
    R = ['1-y', '2-x', '3y', '4-z', '5-x', '6z', '7-y', '8-z', '9x', '10y', '11-x', '12z', '13-x', '14-z', '15x', '16-y', '17-x'] ;
    R = ['1-y', '2-z', '3y', '4-x', '5-z', '6x', '7-y', '8-x', '9z', '10y', '11-z', '12x', '13-z', '14-x', '15z', '16-y', '17-z'] ;
    R = ['1-z', '2-x', '3z', '4-y', '5-x', '6y', '7-z', '8-y', '9x', '10z', '11-x', '12y', '13-x', '14-y', '15x', '16-z', '17-x'] ;
    R = ['1-z', '2-y', '3z', '4-x', '5-y', '6x', '7-z', '8-x', '9y', '10z', '11-y', '12x', '13-y', '14-x', '15y', '16-z', '17-y'] ;
    false.

The numbers indicate which segment you need to rotate, and the direction is given in a 3D Cartesian coordinate system via x, y, z, -x, -y, and -z. 